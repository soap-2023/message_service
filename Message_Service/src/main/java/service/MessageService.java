package service;

import entity.Dialog;
import entity.Message;
import entity.User;
import repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public List<Message> getMessagesContainingText(String searchText) {
        return messageRepository.findByMessageTextContaining(searchText);
    }

    public List<Message> getMessagesByDialog(Dialog dialog) {
        return messageRepository.findByDialog(dialog);
    }

    public List<Message> getMessagesBySender(User sender) {
        return messageRepository.findBySender(sender);
    }

    public Message createMessage(Message message) {
        return messageRepository.save(message);
    }

    public Message updateMessage(Message message) {
        return messageRepository.save(message);
    }

    public void deleteMessage(Long id) {
        messageRepository.deleteById(id);
    }
}
