package service;

import entity.Dialog;
import entity.User;
import repository.DialogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DialogService {

    @Autowired
    private DialogRepository dialogRepository;

    public List<Dialog> getDialogsByParticipant(User user) {
        return dialogRepository.findByParticipantsUser(user);
    }

    public Dialog createDialog(Dialog dialog) {
        return dialogRepository.save(dialog);
    }

    public Dialog updateDialog(Dialog dialog) {
        return dialogRepository.save(dialog);
    }

    public void deleteDialog(Long id) {
        dialogRepository.deleteById(id);
    }
}
