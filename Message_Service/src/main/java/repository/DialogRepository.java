package repository;

import entity.Dialog;
import entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DialogRepository extends JpaRepository<Dialog, Long> {
    List<Dialog> findByParticipantsUser(User user);
}

