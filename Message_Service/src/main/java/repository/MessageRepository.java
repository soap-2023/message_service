package repository;

import entity.Dialog;
import entity.Message;
import entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findByMessageTextContaining(String searchText);

    List<Message> findByDialog(Dialog dialog);
    List<Message> findBySender(User sender);
}

