package repository;

import entity.Attachment;
import entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
    List<Attachment> findByMessage(Message message);
    void deleteByMessage(Message message);
}

