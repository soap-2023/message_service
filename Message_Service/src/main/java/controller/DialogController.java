package controller;

import entity.Dialog;
import entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.DialogService;

import java.util.List;

@RestController
@RequestMapping("/dialogs")
public class DialogController {

    @Autowired
    private DialogService dialogService;

    @GetMapping("/user/{userId}")
    public List<Dialog> getDialogsByParticipant(@PathVariable Long userId) {
        User user = new User();
        user.setId(userId);
        return dialogService.getDialogsByParticipant(user);
    }

    @PostMapping
    public Dialog createDialog(@RequestBody Dialog dialog) {
        return dialogService.createDialog(dialog);
    }

    @PutMapping("/{id}")
    public Dialog updateDialog(@PathVariable Long id, @RequestBody Dialog dialog) {
        dialog.setId(id);
        return dialogService.updateDialog(dialog);
    }

    @DeleteMapping("/{id}")
    public void deleteDialog(@PathVariable Long id) {
        dialogService.deleteDialog(id);
    }
}
