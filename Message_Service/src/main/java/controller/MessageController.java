package controller;

import entity.Dialog;
import entity.Message;
import entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.MessageService;

import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/search")
    public List<Message> getMessagesContainingText(@RequestParam String searchText) {
        return messageService.getMessagesContainingText(searchText);
    }

    @GetMapping("/dialog/{dialogId}")
    public List<Message> getMessagesByDialog(@PathVariable Long dialogId) {
        Dialog dialog = new Dialog();
        dialog.setId(dialogId);
        return messageService.getMessagesByDialog(dialog);
    }

    @GetMapping("/user/{userId}")
    public List<Message> getMessagesBySender(@PathVariable Long userId) {
        User user = new User();
        user.setId(userId);
        return messageService.getMessagesBySender(user);
    }

    @PostMapping
    public Message createMessage(@RequestBody Message message) {
        return messageService.createMessage(message);
    }

    @PutMapping("/{id}")
    public Message updateMessage(@PathVariable Long id, @RequestBody Message message) {
        message.setId(id);
        return messageService.updateMessage(message);
    }

    @DeleteMapping("/{id}")
    public void deleteMessage(@PathVariable Long id) {
        messageService.deleteMessage(id);
    }
}

