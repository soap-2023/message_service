package entity;

import jakarta.persistence.*;

import java.awt.*;


@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender_id", nullable = false)
    private User sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id", nullable = false)
    private User recipient;

    private String messageText;

    @ManyToOne
    @JoinColumn(name = "dialog_id")
    private Dialog dialog;

    private boolean isReply;
    private Long originalMessageId;
    private boolean isDeleted;

    // Геттеры
    public Long getId() {
        return id;
    }

    public User getSender() {
        return sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public String getMessageText() {
        return messageText;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public boolean isReply() {
        return isReply;
    }

    public Long getOriginalMessageId() {
        return originalMessageId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    // Сеттеры
    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public void setReply(boolean reply) {
        isReply = reply;
    }

    public void setOriginalMessageId(Long originalMessageId) {
        this.originalMessageId = originalMessageId;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public void setId(Long id) {
    this.id = id;
    }
}
