package entity;

import jakarta.persistence.*;

@Entity
@Table(name = "attachments")
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "message_id", nullable = false)
    private Message message;

    private String attachmentType;
    private String filePath;

    // Геттеры
    public Long getId() {
        return id;
    }

    public Message getMessage() {
        return message;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public String getFilePath() {
        return filePath;
    }

    // Сеттеры
    public void setMessage(Message message) {
        this.message = message;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}

