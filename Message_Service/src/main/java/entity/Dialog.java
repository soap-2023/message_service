package entity;

import jakarta.persistence.*;


@Entity
@Table(name = "dialogs")
public class Dialog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String dialogName;

    // Геттеры
    public Long getId() {
        return id;
    }

    public String getDialogName() {
        return dialogName;
    }

    // Сеттеры
    public void setDialogName(String dialogName) {
        this.dialogName = dialogName;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
